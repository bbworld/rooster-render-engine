# Welcome to the Rooster Render Engine repository!

**What is Rooster, you ask?**

Rooster is a complete rasterizing PBR renderer, able to take in Blender files and render them at lightning speed. What makes Rooster different than Eevee is its revolutionary hybrid model, which combines the base rasterized image with raytraced lighting, ambient occlusion and other effects, so that you get the full benefits of a raytracing engine at the blazing fast speed of a rasterizer.
